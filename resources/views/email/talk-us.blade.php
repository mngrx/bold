<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Fale conosco - Email</title>
</head>
<body>
    <h1>Bold - online courses</h1>
    <h2>Email de contato - Pelo Fale Conosco</h2>

    <table>
        <tr>
            <td>Enviado por:</td>
            <td>{{ $name }} {{ $lastname }}</td>
        </tr>
        <tr>
            <td>Email:</td>
            <td>{{ $email }}</td>
        </tr>
        <tr>
            <td>Telefone:</td>
            <td>{{ $phone }}</td>
        </tr>
        <tr>
            <td>Mensagem:</td>
            <td>{{ $content }}</td>
        </tr>
    </table>
</body>
</html>
