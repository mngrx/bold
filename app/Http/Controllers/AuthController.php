<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Throwable;
use Exception;

class AuthController extends Controller
{
    /**
     *  Registra um usuário
     *
     *  Função responsável por registrar um usuário no sistema.
     *  @param Request $request
     *  @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        // Validação dos campos principais para o registro
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:6',
            'name' => 'required',
            'email' => 'required|email',
        ]);

        $resposta = [
            'status' => TRUE,
            'message' => 'Cadastrado com sucesso! Faça login para acessar o sistema.'
        ];

        // Verifica se a validação deu certo
        if ($validator->fails()) {

            $mensagem = 'Verifique os seguintes itens: ';

            $mensagem .= $validator->errors()->first();

            $resposta = [
                'status' => FALSE,
                'message' => $mensagem
            ];

            return response()->json($resposta, 422);

        }

        DB::beginTransaction();
        try {

            // Array com dados de acesso do usuário (encripta a senha também)
            $accessData = [
                'password' => md5($request->password),
                'email' => $request->email,
                'name' => $request->name,
            ];

            $newUser = User::create($accessData);

            DB::commit();
        } catch (Throwable $e) {

            DB::rollBack();

            $resposta = [
                'status' => FALSE,
                'message' => $e->getMessage()
            ];

            return response()->json($resposta, 401);
        }

        return response()->json($resposta, 201);
    }

    /**
     * Recupera o token de acesso que representa o 'login' do usuário no sistema
     *
     * Função responsável por 'logar' um usuário no sistema.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        // Tenta autenticar o usuário pelo email e senha
        $user = User::where('email', $request->email)->where('password', md5($request->password))->first();

        if (!$user) {
            $resposta = [
                'status' => FALSE,
                'message' => 'Login ou senha incorretos.'
            ];
            return response()->json($resposta, 404);
        }

        if ($user->id) {

            Auth::loginUsingId($user->id);

            // Recupera o usuário logado
            $user = Auth::user();

            // Recupera informação do token do usuário, gerando o novo
            $resposta = [
                'status' => TRUE,
                'message' => 'Login realizado com sucesso.',
                'token' => $user->createToken('bearer')->accessToken
            ];

            // Retorna o token e o código de sucesso
            return response()->json($resposta, 200);
        } else {
            $resposta = [
                'status' => FALSE,
                'message' => 'Não autorizado.'
            ];
            return response()->json($resposta, 401);
        }
    }

    /**
     * Remove todos os tokens do usuário
     *
     * Função responsável por 'deslogar' um usuário no sistema.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request) {

        if (Auth::check()) {

            User::logoutApi();

            return response()->json(['message' => "Logout realizado com sucesso."], 200);
        }
        // Retorna mensagem caso dê error
        return response()->json(['message' => 'Não autorizado.'], 401);
    }

}
