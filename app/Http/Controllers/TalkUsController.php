<?php

namespace App\Http\Controllers;

use App\FaleConosco;
use App\Mail\MailTalkUs;
use App\TalkUs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Throwable;

/**
 * Controller para gerenciar as operações de Fale Conosco
 *
 * @author Igor Barbosa Nogueira <igornogueir@gmail.com>
 * @version 1.0
 */
class TalkUsController extends Controller
{
    public function all() {

        $mensagens = FaleConosco::all();

        return view('fale-conosco-lista')->with(['mensagens' => $mensagens]);
    }

    /**
     * Representa o envio da mensagem feita pelo usuário
     *
     * Irá receber os dados da mensagem enviada e redirecionará para a página inicial
     *
     * @author Igor Barbosa Nogueira <igornogueir@gmail.com>
     * @param Request $request
     * @return void
     */
    public function create(Request $request) {

        $dados = $request->all();

        $dados = [
            'name' => $dados['name'] ?? '' ,
            'lastname' => $dados['lastname'] ?? '' ,
            'email' => $dados['email'] ?? '',
            'phone' => $dados['phone'] ?? '',
            'message' => $dados['message'] ?? ''
        ];

        $resposta = [];

        $validator = Validator::make(
            $dados,
            [
                'name' => 'required',
                'lastname' => '',
                'email' => 'email',
                'phone' => 'celular_com_ddd',
                'message' => 'required'
            ]
        );

        if($validator->fails()) {

            $mensagem = 'Verifique os seguintes itens: ';

            $mensagem .= $validator->errors()->first();

            $resposta = [
                'status' => FALSE,
                'message' => $mensagem
            ];

            return response()->json($resposta, 422);
        }
        DB::beginTransaction();
        try {

            $talkUs = TalkUs::create($dados);

            $resposta = [
                'status' => TRUE,
                'message' => 'Sua mensagem foi enviada com sucesso!'
            ];

            $dados['content'] = $dados['message'];

            Mail::to('d3soft@d3soft.com.br')->send(new MailTalkUs($dados));

            DB::commit();

            return response()->json($resposta, 201);
        } catch(Throwable $e) {
            DB::rollBack();
            $resposta = [
                'status' => FALSE,
                'message' => 'Por favor, tente novamente mais tarde.'
            ];
            return response()->json($resposta, 500);
        }

    }
}
