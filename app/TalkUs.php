<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TalkUs extends Model
{
    protected $table = 'TalkUs';

    protected $connection = 'mysql';

    protected $primaryKey = 'id';

    // Para poder utilizar o mass assignment sem problemas
    protected $guarded = [];
}
