<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/talkus', 'TalkUsController@create');


/* Rotas relacionadas com acesso usuários */
Route::post('login', 'AuthController@login');
Route::post('register', 'AuthController@register');
Route::group(['prefix' => 'user', 'middleware' => 'auth:api'], function () {
    Route::get('', function (Request $request) {
        return Auth::user();
    });
    Route::get('logout', 'AuthController@logout');
});
